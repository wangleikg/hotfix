// +build linux,cgo,amd64 freebsd,cgo,amd64 darwin,cgo,amd64

package hotfix

/*
   #cgo linux LDFLAGS: -ldl
   #include <dlfcn.h>
   #include <limits.h>
   #include <stdlib.h>
   #include <stdint.h>

   #include <stdio.h>
   #ifndef RTLD_MAIN_ONLY
   #define RTLD_MAIN_ONLY 0
   #endif

   static void* localLookup(const char* name, char** err) {
   	void* r = dlsym(RTLD_MAIN_ONLY, name);
   	if (r == NULL) {
   		*err = (char*)dlerror();
   	}
   	return r;
   }
*/
import "C"
import (
	"errors"
	"unsafe"

	"bitbucket.org/wangleikg/hotfix/forceexport"
)

func LookupLocalFunction(name string) (unsafe.Pointer, error) {
	var cErr *C.char
	nameStr := make([]byte, len(name)+1)
	copy(nameStr, name)

	handle := C.localLookup((*C.char)(unsafe.Pointer(&nameStr[0])), &cErr)
	if handle == nil {
		return nil, errors.New(C.GoString(cErr))
	}
	return handle, nil
}

func LookupLocalUnexportedFunction(name string) (unsafe.Pointer, error) {
	ptr, err := forceexport.FindFuncWithName(name)
	return unsafe.Pointer(ptr), err
}
