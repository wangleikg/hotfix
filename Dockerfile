FROM golang:1.17
WORKDIR /go/src/
COPY ./ /go/src/
RUN go get -d -v ./...
WORKDIR /go/src/
ENTRYPOINT cd /go/src/example/hotfix && ./run.sh
