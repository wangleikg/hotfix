# README #

Only Support linux,freebsd Now!!!
注意: plugin模式会进行类型检查，对于local更新以及指定地址更新需要自行保证函数参数返回值一致，暂时未进行检查，如果错误程序将宕线。
最新版本的macOS不支持，因为对于mprotect进行了安全控制，早期版本可以, 可以通过设置生成的二进制程序控制字段实现。
```shell
printf '\x07' | dd of=$1 bs=1 seek=160 count=1 conv=notrun
```

### Implementation ###
    使用修改代码段的方式实现，所以理论上不是完全安全（由于现在的amd64架构的cacheline，经过压测没有发现问题），主要用于紧急修改bug。
    对于导出对象的非导出成员变量，需要使用偏移的方式实现或者强制转换。对于未导出的函数可以调用，也可以更新，但是有限制，比如如果太简单的函数需要强制noinline，必须至少调用过一次等。
    Warning: 一定要保证参数和返回值一致，除了plugin模式不进行类型检查！！！（TODO，可以使用forceexpor进行类型检查，目前暂未实现）
    对于必定要更新的函数可以使用go:noinline标记保证可以热更新。
    
```go
    package example

    type Role struct{
    }
    
    //Set 可以更新
    //go:noinline
    func(r *Role)Set(x ...interface{})error{
        return nil
    }
    
    //patch Set的函数可以如下定义
    //对于成员函数，使用第一个参数为导出类型即可
    func PatchRoleSet(r *Role,x ...interface{})error{
        return nil
    }
    
    //不可更新
    func(r *Role)set(x ...int)error{
        return nil
    }
```
### Example ###
    ./run.sh
### Who ###

* lei.wang@kingsgroupgames.com
