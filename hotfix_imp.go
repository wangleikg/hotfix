package hotfix

import (
	"fmt"
	"plugin"
	"reflect"
)

var log func(format string, a ...interface{}) (int, error)

func init() {
	log = func(format string, a ...interface{}) (int, error) {
		if !OpenLog {
			return 0, nil
		}
		return fmt.Printf("[HOTFIX_DEBUG]"+format, a...)
	}
}

func copySymbol(symbol plugin.Symbol, out interface{}) error {
	outValue := reflect.ValueOf(out)
	if outValue.Type().Kind() != reflect.Ptr {
		return fmt.Errorf("%v is not pointer", out)
	}
	symValue := reflect.ValueOf(symbol)
	t1, t2 := symValue.Type(), outValue.Elem().Type()
	if t1.AssignableTo(t2) {
		outValue.Elem().Set(symValue)
	} else if t1.Kind() == reflect.Ptr && t1.Elem().AssignableTo(t2) {
		outValue.Elem().Set(symValue.Elem())
	} else {
		return fmt.Errorf("%s: (%s) is not assignable to *out (%s)", outValue.Type().Name(), t1, t2)
	}
	return nil
}

func (h *Hotfix) Patch(path string) (*Patcher, error) {
	lib, err := plugin.Open(path)
	if err != nil {
		return nil, err
	}
	sym, err := lib.Lookup(MapName)
	if err != nil {
		return nil, err
	}
	var hotfix HotfixMap
	if err = copySymbol(sym, &hotfix); err != nil {
		return nil, err
	}

	h.Lock()
	defer h.Unlock()

	fixer := newPatcher(h, path)

	var errFunc string
	for _, u := range hotfix {
		cur := u
		fixer.add(&cur)
		nErr := (&cur).patch(h)
		if nErr != nil {
			errFunc = errFunc + nErr.Error() + "\n"
			continue
		}
	}
	if len(errFunc) != 0 {
		return fixer, fmt.Errorf("plugin failed:%s", errFunc)
	}
	return fixer, nil
}

func (h *Hotfix) RecoverAll() int {
	h.Lock()
	defer h.Unlock()

	ret := 0
	for ptr, orig := range h.origin {
		if err := patch(ptr, orig); err != nil {
			ret++
		}
	}
	h.origin = make(map[uintptr][]byte)
	h.fixer = h.fixer[:]
	return ret
}

func (h *Hotfix) onPatch(name string, src uintptr, origin []byte) {
	if _, ok := h.origin[src]; ok {
		return
	}
	h.origin[src] = origin
}

func (h *Hotfix) SetPluginFinder(handler func(string) (*plugin.Plugin, error)) {
	h.Lock()
	h.handler = handler
	h.Unlock()
}
