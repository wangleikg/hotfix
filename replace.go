package hotfix

import (
	"errors"
	"fmt"
	"reflect"
	"sync"
	"syscall"
	"unsafe"
)

func makeByte(p uintptr, length int) []byte {
	return *(*[]byte)(unsafe.Pointer(&reflect.SliceHeader{
		Data: p,
		Len:  length,
		Cap:  length,
	}))
}

func makeJumpCode(to uintptr) []byte {
	return []byte{
		0x48, 0xBA,
		byte(to),
		byte(to >> 8),
		byte(to >> 16),
		byte(to >> 24),
		byte(to >> 32),
		byte(to >> 40),
		byte(to >> 48),
		byte(to >> 56),
		0xFF, 0x22,
	}
}
func replaceFunction(src, dst uintptr) (original []byte, err error) {
	log("replace function jump from:0x%x ---> 0x%x", src, dst)
	code := makeJumpCode(dst)
	f := makeByte(src, len(code))
	original = make([]byte, len(f))
	copy(original, f)
	err = patch(src, code)
	if err != nil {
		return nil, err
	}
	return
}

func patchInterface(target, replacement interface{}) (orig []byte, err error) {
	t := reflect.ValueOf(target)
	r := reflect.ValueOf(replacement)
	return patchValue(t, r)
}

func patchValue(src, replacement reflect.Value) (orig []byte, err error) {
	if src.Kind() != reflect.Func {
		return orig, errors.New("src has to be a Func")
	}

	if replacement.Kind() != reflect.Func {
		return orig, errors.New("replacement has to be a Func")
	}

	if src.Type() != replacement.Type() {
		return orig, errors.New(fmt.Sprintf("src and replacement have to have the same type %s != %s",
			src.Type(), replacement.Type()))
	}

	orig, err = replaceFunction(src.Pointer(), (uintptr)(getRawPtr(replacement)))
	return orig, err
}

func mprotect(addr uintptr, length int, prot int) error {
	pageSize := syscall.Getpagesize()
	p := addr & ^(uintptr(pageSize - 1))
	for ; p < addr+uintptr(length); p += uintptr(pageSize) {
		page := makeByte(p, pageSize)
		err := syscall.Mprotect(page, prot)
		if err != nil {
			return err
		}
	}
	return nil
}

var mProtectLocker sync.Mutex

func patch(location uintptr, data []byte) error {
	f := makeByte(location, len(data))
	mProtectLocker.Lock()
	if err := mprotect(location, len(data), syscall.PROT_READ|syscall.PROT_WRITE|syscall.PROT_EXEC); err != nil {
		return err
	}
	copy(f, data)
	mprotect(location, len(data), syscall.PROT_READ|syscall.PROT_EXEC)
	mProtectLocker.Unlock()

	return nil
}
