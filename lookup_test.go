package hotfix

import (
	"fmt"
	"testing"
	"time"
)

func TestPatch(t *testing.T) {
	_, err := patchInterface(time.Now, func() time.Time {
		return time.Date(2000, time.January, 1, 0, 0, 0, 0, time.UTC)
	})
	t.Log(err)
	fmt.Println(time.Now())
}

func fixLoadPlugin(path string) error {
	fmt.Println(path + "--FIX")
	return nil
}

func TestLookUpLocalFunction(t *testing.T) {
	t.Log(LookupLocalFunction("main"))
	t.Log(LookupLocalFunction("bitbucket.org/wangleikg/hotfix.TestLookUpLocalFunction"))
}
