package hotfix

import (
	"plugin"
	"sync"
)

var OpenLog bool

type Hotfix struct {
	sync.Mutex
	fixer   []*Patcher
	origin  map[uintptr][]byte
	handler func(string) (*plugin.Plugin, error)
}

func NewHotfix() *Hotfix {
	return &Hotfix{
		origin: make(map[uintptr][]byte),
	}
}

/*plugin need Create an global map "var HotfixMap hotfix.HotfixMap" to register the patch info
 */
const (
	MapName = "HotfixMap"
)

type HotfixMap map[string]Replacement

func NewLocalFunctionReplacement(targetName string, newFunc interface{}) Replacement {
	return Replacement{
		SrcName: targetName,
		DstFunc: newFunc,
	}
}

func NewPluginFunctionReplacement(targetName string, pluginName string, newFunc interface{}) Replacement {
	return Replacement{
		SrcName:    targetName,
		pluginName: pluginName,
		DstFunc:    newFunc,
	}
}

func NewRawFunctionPointerReplacement(targetName string, src uintptr, newFunc interface{}) Replacement {
	return Replacement{
		SrcName:    targetName,
		DstFunc:    newFunc,
		SrcFuncPtr: src,
	}
}
