package hotfix

import (
	"errors"
	"fmt"
	"plugin"
	"reflect"
	"sync"
	"time"
	"unsafe"
)

type Replacement struct {
	DstFunc    interface{}
	SrcName    string
	SrcFuncPtr uintptr
	originCode []byte
	patched    bool
	pluginName string
	unexported bool
}

func (u Replacement) SetUnexported() Replacement {
	u.unexported = true
	return u
}

func getRawPtr(v reflect.Value) unsafe.Pointer {
	return (*(*[2]unsafe.Pointer)(unsafe.Pointer(&v)))[1]
}

func (u *Replacement) Dump() string {
	return fmt.Sprintf("Patch:%s,patched:%v,plugin:%s,unexported:%v,ptr:0x%x",
		u.SrcName, u.patched, u.pluginName, u.unexported, u.SrcFuncPtr)
}

func (u *Replacement) preparePlugin(h *Hotfix) (reflect.Value, error) {
	if len(u.pluginName) == 0 {
		return reflect.Value{}, errors.New("no plugin name set")
	}
	handler := h.handler
	if handler == nil {
		handler = plugin.Open
	}
	plugin, err := handler(u.pluginName)
	if plugin == nil {
		return reflect.Value{}, err
	}
	sym, err := plugin.Lookup(u.SrcName)
	if err != nil {
		return reflect.Value{}, err
	}
	u.SrcFuncPtr = reflect.ValueOf(sym).Pointer()
	return reflect.ValueOf(sym), nil
}

func (u *Replacement) prepareRawPointer(h *Hotfix) (reflect.Value, error) {
	if u.SrcFuncPtr == 0 {
		return reflect.Value{}, errors.New("raw pointer is null")
	}
	src := reflect.ValueOf(u.DstFunc)
	val := (*[2]unsafe.Pointer)(unsafe.Pointer(&src))
	(*val)[1] = unsafe.Pointer(&u.SrcFuncPtr)
	return src, nil
}

func (u *Replacement) Prepare(h *Hotfix) (reflect.Value, error) {
	if len(u.pluginName) > 0 {
		return u.preparePlugin(h)
	}
	if u.SrcFuncPtr != 0 {
		return u.prepareRawPointer(h)
	}
	return u.prepareLocal(h)
}

func (u *Replacement) prepareLocal(h *Hotfix) (reflect.Value, error) {
	handle, err := LookupLocalFunction(u.SrcName)
	if err != nil {
		if u.unexported {
			handle, err = LookupLocalUnexportedFunction(u.SrcName)
		}
		if err != nil {
			return reflect.Value{}, err
		}
	} else {
		u.unexported = false
	}
	u.SrcFuncPtr = uintptr(handle)
	return u.prepareRawPointer(h)
}

func (u *Replacement) patch(h *Hotfix) error {
	src, err := u.Prepare(h)
	if err != nil {
		return err
	}
	u.originCode, err = patchValue(src, reflect.ValueOf(u.DstFunc))

	log("====Patch:[%s] ptr:0x%x->%x, origin code:%+v, err:%v\n",
		u.SrcName, u.SrcFuncPtr, (uintptr)(getRawPtr(reflect.ValueOf(u.DstFunc))), u.originCode, err)
	if err == nil {
		u.patched = true
		h.onPatch(u.SrcName, u.SrcFuncPtr, u.originCode)
	}
	return err
}

func (u *Replacement) Recover(_ *Hotfix) error {
	if !u.patched {
		return fmt.Errorf("func:%s not patched", u.SrcName)
	}
	if len(u.originCode) == 0 {
		return fmt.Errorf("func:%s have no origin", u.SrcName)
	}
	if u.SrcFuncPtr == 0 {
		return fmt.Errorf("func:%s have no srcptr", u.SrcName)
	}

	u.patched = false
	log("====Recover [%s] ptr:0x%x, with code:%+v\n", u.SrcName, u.SrcFuncPtr, u.originCode)
	return patch(u.SrcFuncPtr, u.originCode)
}

type Patcher struct {
	sync.Mutex
	index int
	h     *Hotfix
	time  time.Time
	Path  string
	units map[string]*Replacement
}

func newPatcher(h *Hotfix, path string) *Patcher {
	ret := &Patcher{h: h, index: len(h.fixer), time: time.Now(), Path: path, units: make(map[string]*Replacement)}
	h.fixer = append(h.fixer, ret)
	return ret
}

func (f *Patcher) add(u *Replacement) {
	f.Lock()
	f.units[u.SrcName] = u
	f.Unlock()
}

func (f *Patcher) Recover() (n int, err error) {
	f.Lock()
	defer f.Unlock()

	var errs string
	for _, u := range f.units {
		if e := u.Recover(f.h); e != nil {
			errs += e.Error() + "\n"
			continue
		}
		n++
	}
	if len(errs) != 0 {
		return n, errors.New("RecoverErrors:" + errs)
	}
	return n, nil
}

func (f *Patcher) Dump() string {

	f.Lock()
	defer f.Unlock()

	ret := fmt.Sprintf("Patcher[total:%d]\n", len(f.units))
	for _, r := range f.units {
		ret += "======" + r.Dump() + "\n"
	}
	return ret
}
