package main

import (
	"bitbucket.org/wangleikg/hotfix/example/unexported/role"
	"fmt"

	ht "bitbucket.org/wangleikg/hotfix"
)

var (
	r      = role.Role{}
	hotfix = ht.NewHotfix()
)

func main() {
	r.SetName("OK")
	r.Print()

	patcher, err := hotfix.Patch("patch.so")
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("Patched")
		fmt.Println(patcher.Dump())
	}
	r.SetName("OK")
	r.Print()
}
