package role

import (
	"bitbucket.org/wangleikg/hotfix/example/unexported/role/internal"
	"fmt"
)

type Role struct {
	Name string
	orig string
}

//go:noinline
func (r *Role) Print() {
	r.setOrig(1, 2, r.orig)
	fmt.Println(r.orig, r.Name)
}

//go:noinline
func (r *Role) setName(n string) error {
	r.Name = n + r.orig
	return nil
}

//go:noinline
func (r *Role) SetName(n string) error {
	return r.setName(n)
}

//go:noinline
func (r *Role) setOrig(n int, p float32, o string) {
	r.orig = o
}

var Test interface{}

func init() {
	Test = &internal.Role{}
}
