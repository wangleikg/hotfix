package internal

import "fmt"

type Role struct {
	Name string
	orig string
}

//go:noinline
func (r *Role) Print() {
	r.setOrig(r.orig)
	fmt.Println(r.orig, r.Name)
}

//go:noinline
func (r *Role) setName(n string) error {
	fmt.Println("internal func:setName")
	r.Name = n + r.orig
	return nil
}

//go:noinline
func (r *Role) SetName(n string) error {
	fmt.Println("internal func:SetName")
	return r.setName(n)
}

//go:noinline
func (r *Role) setOrig(o string) {
	r.orig = o
}
