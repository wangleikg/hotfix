package main

import (
	"bitbucket.org/wangleikg/hotfix/example/unexported/role"
	"errors"
	"fmt"
	"sync"
	"unsafe"

	"bitbucket.org/wangleikg/hotfix"
	"bitbucket.org/wangleikg/hotfix/forceexport"
)

var (
	setNamePath = "bitbucket.org/wangleikg/hotfix/example/unexported/role.(*Role).setName"
	setName     func(*role.Role, string) error
	once        sync.Once
)

func SetName(r *role.Role, n string) error {
	fmt.Println("hotfix func:SetName")
	once.Do(func() {
		err := forceexport.GetFunc(&setName, setNamePath)
		if err != nil {
			fmt.Println("Cant load:", err)
		}
	})
	if setName == nil {
		return errors.New("setName cant load")
	}
	setName(r, "PatchSetName:"+n)
	return nil
}

type Role2 struct {
	Name string
	orig string
}

func SetOrig(r *role.Role, n int, p float32, ns string) {
	rr := (*Role2)(unsafe.Pointer(r))
	rr.orig = "test"
	fmt.Println("hotfix:func:SetOrig,", n, p)
}

//必须定义替换引导Map，init的时候注册
var HotfixMap hotfix.HotfixMap

func init() {
	role := "bitbucket.org/wangleikg/hotfix/example/unexported/role.(*Role)."
	HotfixMap = make(hotfix.HotfixMap)
	HotfixMap["SetName"] = hotfix.NewLocalFunctionReplacement(
		role+"SetName", SetName)
	HotfixMap["setOrig"] = hotfix.NewLocalFunctionReplacement(
		role+"setOrig", SetOrig).SetUnexported()
}
