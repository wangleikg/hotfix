package main_test

import (
	"bitbucket.org/wangleikg/hotfix/example/unexported/role"
	"bitbucket.org/wangleikg/hotfix/forceexport"
	"bou.ke/monkey"
	"fmt"
	_ "plugin"
	"testing"
	_ "unsafe"
)

func TestGetFunc(t *testing.T) {
	r := role.Role{}
	r.SetName("t")
	r.Print()

	var setName = "bitbucket.org/wangleikg/hotfix/example/unexported/role.(*Role).setName"
	t.Log(forceexport.FindFuncWithName(setName))

	var f func(r *role.Role, n string) error

	t.Log(r.Name)
	err := forceexport.GetFunc(&f, setName)
	if err == nil {
		f(&r, "gogo")
	} else {
		t.Log(err)
	}

	t.Log(r.Name)

	publicStructPrivateFunc(&r, "run")
	t.Log(r.Name)

	monkey.Patch(publicStructPrivateFunc, func(r *role.Role, n string) error {
		fmt.Println("Patch Do Nothing:", n)
		return nil
	})

	err = forceexport.GetFunc(&f, "bitbucket.org/wangleikg/hotfix/example/unexported/role/internal.(*Role).SetName")
	if err == nil {
		f(&r, "gogo2")
	} else {
		t.Log(err)
	}
	t.Log(r.Name)
}

func TestInternal(t *testing.T) {
	t.Log(forceexport.FindFuncWithName("a"))
	t.Log(forceexport.FindFuncWithName("bitbucket.org/wangleikg/hotfix/example/unexported/role/internal.(*Role).setName"))
}

//go:linkname publicStructPrivateFunc bitbucket.org/wangleikg/hotfix/example/unexported/role/internal.(*Role).SetName
func publicStructPrivateFunc(r *role.Role, n string) error
