package main

import (
	"bitbucket.org/wangleikg/hotfix/example/unexported/role"
	"fmt"
	"runtime"
	"sync"

	ht "bitbucket.org/wangleikg/hotfix"
)

var (
	hotfix = ht.NewHotfix()
)

func main() {
	wg := sync.WaitGroup{}
	wg.Add(runtime.GOMAXPROCS(0))
	go func() {
		r := role.Role{}
		for {
			r.SetName("RUN")
			r.Print()
		}
	}()

	for i := 0; i < runtime.GOMAXPROCS(0); i++ {
		go func() {
			r := role.Role{}
			r.SetName("OK")
			r.Print()
			for i := 0; i < 1000; i++ {
				patcher, err := hotfix.Patch("patch.so")
				if err != nil {
					fmt.Println(err)
				} else {
					fmt.Println("Patched")
					fmt.Println(patcher.Dump())
				}
				r.SetName("OK")
				r.Print()
				if patcher != nil {
					patcher.Recover()
				}
				r.SetName("OK")
				r.Print()
			}
			wg.Done()
		}()
	}
	wg.Wait()
}
