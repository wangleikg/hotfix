#!/usr/bin/env bash
go build -gcflags="-l" -o exe main.go
[[ $? -ne 0 ]] && exit 1
go build -buildmode=plugin -gcflags="-l" -o  patch.so patch/hotfix.go
[[ $? -ne 0 ]] && exit 1
./exe
