#!/usr/bin/env bash
go build -gcflags="-l" -o exe main.go
# shellcheck disable=SC2181
[[ $? -ne 0 ]] && exit 1
go build -buildmode=plugin -gcflags="-l" -o  patch.so patch/hotfix.go
[[ $? -ne 0 ]] && exit 1
go build -buildmode=plugin -gcflags="-l" -o  patch2.so patch2/hotfix.go
[[ $? -ne 0 ]] && exit 1
printf '\x07' | dd of=./exe bs=1 seek=160 count=1 conv=notrunc
./exe
