package main

import (
	"fmt"
	_ "unsafe"

	"bitbucket.org/wangleikg/hotfix"
	"bitbucket.org/wangleikg/hotfix/example/hotfix/role"
)

func FixWorld(r *role.Role) {
	fmt.Println("Wonderful World, r.Name:", r.Name)
}

//go:linkname public_struct_private_func bitbucket.org/wangleikg/hotfix/example/role/role.(*Role).setName
//func public_struct_private_func(r *role.Role, n string) error

func SetName(r *role.Role, n string) error {
	r.Name = "PatchSetName"
	return nil
	//return public_struct_private_func(r, "No")
}

//必须定义替换引导Map，init的时候注册
var HotfixMap hotfix.HotfixMap

func init() {
	role := "bitbucket.org/wangleikg/hotfix/example/hotfix/role.(*Role)."
	HotfixMap = make(hotfix.HotfixMap)
	HotfixMap["World"] = hotfix.NewLocalFunctionReplacement(
		role+"World", FixWorld)
	HotfixMap["setName"] = hotfix.NewLocalFunctionReplacement(
		role+"Set", SetName)
}
