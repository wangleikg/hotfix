package main

import (
	"fmt"
	_ "unsafe"

	"bitbucket.org/wangleikg/hotfix"
	"bitbucket.org/wangleikg/hotfix/example/hotfix/role"
)

func FixWorld(r *role.Role) {
	fmt.Println("Terrible World ", r.Name)
}

//go:linkname public_struct_private_func bitbucket.org/wangleikg/hotfix/example/role/role.(*Role).setName
//func public_struct_private_func(r *role.Role, n string) error

func SetName(r *role.Role, n string) error {
	r.Name = "PatchSetName2"
	return nil
}

//必须定义替换引导Map，init的时候注册
var HotfixMap hotfix.HotfixMap

func init() {
	pluginName := "patch.so"
	HotfixMap = make(hotfix.HotfixMap)
	HotfixMap["World"] = hotfix.NewPluginFunctionReplacement(
		"FixWorld", pluginName, FixWorld)
	HotfixMap["setName"] = hotfix.NewPluginFunctionReplacement(
		"SetName", pluginName, SetName)
}
