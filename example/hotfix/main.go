package main

import (
	"fmt"
	"plugin"
	"reflect"

	ht "bitbucket.org/wangleikg/hotfix"
	"bitbucket.org/wangleikg/hotfix/example/hotfix/role"
)

/*
Output:
[root@localhost example]# ./run.sh
Hello-->World
-->TestPatch: patch.so:FixWorld(&r)

Wonderful World, r.Name: Origin
<---TestPatch
Patched
Hello-->Wonderful World, r.Name: Origin
Set OK
Hello-->Wonderful World, r.Name: PatchSetName
Patched2
Hello-->Terrible World  PatchSetName
Set OK2
Hello-->Terrible World  PatchSetName2
-->TestPatch: patch.so:FixWorld(&r)

Terrible World  PatchSetName2
<---TestPatch
-->Recover2: 2
-->TestPatch: patch.so:FixWorld(&r)

Wonderful World, r.Name: PatchSetName2
<---TestPatch
Set OK2
Hello-->Wonderful World, r.Name: PatchSetName
-->Recover: 2
Hello-->World
*/

var (
	r      = role.Role{}
	hotfix = ht.NewHotfix()
)

func TestPatch() {
	fmt.Println("-->TestPatch: patch.so:FixWorld(&r)")
	defer fmt.Println("<---TestPatch")

	p, err := plugin.Open("patch.so")
	if err != nil {
		fmt.Println(err)
		return
	}
	sym, err := p.Lookup("FixWorld")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(reflect.TypeOf(sym).Name())
	if f, ok := sym.(func(r *role.Role)); ok {
		(f)(&r)
	} else {
		fmt.Printf("wrong type:%+v\n", sym)
	}
}

func main() {
	r.Name = "Origin"
	r.Hello()
	r.World()
	TestPatch()

	fixer, err := hotfix.Patch("patch.so")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("Patched")
	r.Hello()
	r.World()

	fmt.Println("Set OK")
	r.Set("OK")
	r.Hello()
	r.World()

	defer func() {
		if fixer == nil {
			return
		}
		n, err := fixer.Recover()
		fmt.Println("-->Recover:", n)
		if err != nil {
			fmt.Println(err)
		}
		r.Set("OK")
		r.Hello()
		r.World()
	}()

	fixer2, err := hotfix.Patch("patch2.so")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("Patched2")
	r.Hello()
	r.World()

	fmt.Println("Set OK2")
	r.Set("OK2")
	r.Hello()
	r.World()

	if fixer2 == nil {
		return
	}

	TestPatch()

	n, err := fixer2.Recover()
	fmt.Println("-->Recover2:", n)
	if err != nil {
		fmt.Printf("Recover failed:%v\n", err)
		return
	}
	TestPatch()
	fmt.Println("Set OK2")
	r.Set("OK2")
	r.Hello()
	r.World()
}
