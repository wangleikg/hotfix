package role

import "fmt"

type Role struct {
	Name string
}

func (r *Role) Hello() {
	fmt.Print("Hello-->")
}

func (r *Role) World() {
	fmt.Println("World")
}

func (r *Role) setName(n string) error {
	r.Name = n
	return nil
}

func (r *Role) Set(n string) error {
	return r.setName(n)
}
